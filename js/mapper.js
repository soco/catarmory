/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Mapper class
 * @class Mapper
 * @singleton
 */

Mapper = (function(o) {
	var Mapper = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Mapper.prototype, {
		/**
		 * Initialize Mapper
		 * @param {Object} jQuery parent
		 * @param {Object} NPC/Gameobject
		 */
		init: function($el,o) {
			this.entry = o.entry;
			this.$el = $el;
			this.data = {};
			this.spawns = [];
			
			var scope = this;
			
			// fetch basic quest data
			Main.fetch(
				{
					what: 'npc',
					action: 'spawns',
					id: this.entry
				}, function(response) {
					this.data = response.data;

					this.$wrapper = $('<div id="mapper-wrapper" style="position: relative"></div>');
					this.$zones = $('<div class="zone-list">This npc can be found in these zones: </div>');
					var firstZone = null;

					for (var i in this.data) {
						var $zoneName = $('<span data-value="'+i+'">'+L12N('zone_'+i)+' ('+this.data[i].length+')</span>');
						$zoneName.click(function() {
							var zone = $(this).attr('data-value');
							scope._makeMap(zone,$(this));
						});
						this.$zones.append( $zoneName );
						this.$zones.append( ', ');

						if (!firstZone)
							firstZone = $zoneName;
					}

					this.$wrapper.append(this.$zones);
					

					this.$spawns = $('<div style="position: relative;"></div>');
					this.$wrapper.append(this.$spawns);

					this.$map = $('<div class="map"></div>');
					this.$spawns.append(this.$map);

					
					this.$el.html(this.$wrapper);

					if (firstZone) {
						var zone = firstZone.attr('data-value');
						scope._makeMap(zone,firstZone);
					}
				}, this
			);
		},

		/**
		* Init basic stuff - render
		*/
		_makeMap: function(zone,$el) {
			
			$('span',this.$zones).removeClass('active');
			$el.addClass('active');
			
			this.$map.html('<img src="maps/'+zone+'.jpg" width="772" height="515">');
			
			// remove previously displayed spawns pins
			for (var i in this.spawns) {
				this.spawns[i].remove();
			}
			
			for (var j in this.data[zone]) {
				var spawn = new Spawn(this.data[zone][j],this);
				spawn.append();
				this.spawns[j] = spawn;
			}
		}
	});
	return Mapper;
})();
  




/**
 * Spawn class
 * @class Spawn
 * @singleton
 */

Spawn = (function(o) {
	var Spawn = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Spawn.prototype, {
		/**
		 * Initialize Spawn
		 * @param {Object} spawn data
		 * @param {Object} mapper scope
		 */
		init: function(spawn,mapper) {
			this.mapper = mapper;
			this.$pin = $('<img data-type="spawn" data-guid="'+spawn[0]+'" src="images/pin-yellow.png">');
			this.$pin.css({position: 'absolute', left: spawn[1], top: spawn[2]});
			this.$pin.tooltip();
		},
		
		/**
		 * Append pin to mapper object
		 */
		append: function() {
			this.mapper.$spawns.append(this.$pin);
		},
		
		/**
		 * Remove pin from DOM
		 */
		remove: function() {
			this.$pin.remove();
		}
	});
	return Spawn;
})();
