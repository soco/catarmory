/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 
/**
 * Spell class
 * @class Spell
 * @singleton
 */
Spell = (function(n) {
	var Spell = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Spell.prototype, {
		/**
		 * Initialize Spell
		 * @param {Object} n Spell object data from server
		 */
		init: function(s) {
			this.id = s.id
			this.attr0 = s.attr0;
			this.attr1 = s.attr1;
			this.attr2 = s.attr2;
			this.attr3 = s.attr3;
			this.attr4 = s.attr4;
			this.attr5 = s.attr5;
			this.attr6 = s.attr6;
			this.attr7 = s.attr7;
			this.attr8 = s.attr8;
			this.attr9 = s.attr9;
			this.attr10 = s.attr10;
			this.castTime = s.cast_time;
			this.duration = s.duration;
			this.name = s.name;
			this.rank = s.rank;
			this.description = s.description;
			this.tooltip = s.tooltip;
			this.icon = s.icon;
			this.effects = s.effects;
		},

		/**
		* Init basic stuff - render
		*/
		initBasic: function() {
			
			$('#spell_name').text(this.name);
			$('#spell_icon').html('<img src="/icons/'+this.icon+'.png">');

			$('#spell_rank').text(this.rank);
			$('#spell_description').text(this.description);
			//$('#spell_tooltip').text(this.tooltip);
			
			// spell attributes
			var attributes = [];
			for (var i=0;i<=10;++i) {
				attributes[i] = [];
				for (var j=0;j<=31;++j) {
					if (this['attr'+i] & 1 << j) {
						attributes[i].push(spellAttributes[i][j]);
					}
				}
				$('#spell_attributes'+i).html(attributes[i].join(', '));
			}
			
			// spell effects
			for (var i=0;i<=2;++i) {
				var $effect = $('<div>'+spellEffects[this.effects[i].effect]+': '+spellAuras[this.effects[i].aura]+' ('+this.effects[i].basepoints+')</div>');
				if (this.effects[i].trigger_spell > 0) {
					$spell = $('<div data-type="spell" data-entry="'+this.effects[i].trigger_spell+'"><a href="'+this.effects[i].trigger_spell+'">SPELL</a></div>');
					$spell.tooltip();
					$effect.append($spell);
				}
				
				$('#spell_effect'+i).html($effect);
			}
		}
	});

	return Spell;
})();
  