<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */




class Item extends Cache {

	protected $_item;
	protected $db;

	public $id;
	public $name;
	public $flags;
	public $flags2;
	public $quality;
	public $icon;
	public $score;
	public $class;
	public $subclass;
	public $inventory_type;
	public $item_level;
	public $speed;

	/**
	 * @param PDO database handler
	 * @param integer entry of item
	 */
	function __construct($db,$id) {
		$this->db = $db;
	}

	/**
	 * Get item by guid
	 */
	public function get_by_guid($guid) {
		if ($this->_item = $this->get_cache(array('item_guid',$guid),ITEM_GUID_EXPIRE)) {
			$this->_process();
			return;
		}

		$get_item = $this->db->query('
			SELECT ii.`itemEntry`,ii.`count`,ii.`enchantments`,ii.`owner_guid`,ii.`randomPropertyId`,ii.`durability`,dis.`col_1` AS quality,dis.`col_2` AS flags,dis.`col_3` AS flags2,dis.`col_8` AS SellPrice,dis.`col_9` AS inventoryType,dis.`col_10` AS allowableClass,dis.`col_11` AS allowableRace,dis.`col_12` AS itemLevel,dis.`col_13` AS requiredLevel,dis.`col_14` AS requiredSkill,dis.`col_15` AS requiredSkillRank,dis.`col_16` AS requiredSpell,dis.`col_17` AS requiredHonorRank,dis.`col_18` AS requiredCityRank,dis.`col_19` AS requiredReputationFaction,dis.`col_20` AS requiredReputationRank,dis.`col_23` AS ContainerSlots,dis.`col_24` AS statType_0,dis.`col_25` AS statType_1,dis.`col_26` AS statType_2,dis.`col_27` AS statType_3,dis.`col_28` AS statType_4,dis.`col_29` AS statType_5,dis.`col_30` AS statType_6,dis.`col_31` AS statType_7,dis.`col_32` AS statType_8,dis.`col_33` AS statType_9,dis.`col_34` AS statValue_0,dis.`col_35` AS statValue_1,dis.`col_36` AS statValue_2,dis.`col_37` AS statValue_3,dis.`col_38` AS statValue_4,dis.`col_39` AS statValue_5,dis.`col_40` AS statValue_6,dis.`col_41` AS statValue_7,dis.`col_42` AS statValue_8,dis.`col_43` AS statValue_9,dis.`col_66` AS speed,dis.`col_68` AS spell_1,dis.`col_69` AS spell_2,dis.`col_70` AS spell_3,dis.`col_71` AS spell_4,dis.`col_72` AS spell_5,dis.`col_98` AS bonding,dis.`col_99` AS name,dis.`col_113` AS item_set,dis.`col_118` AS socket_1,dis.`col_119` AS socket_2,dis.`col_120` AS socket_3,dis.`col_124` AS socket_bonus,LOWER(idi.`col_5`) AS icon,di.`col_1` AS itemClass,di.`col_2` AS itemSubClass
			FROM `'.$this->db->characterdb.'`.`item_instance` AS ii
			LEFT JOIN `db2_item_sparse` AS dis ON (ii.`itemEntry`=dis.`col_0`)
			LEFT JOIN `db2_item` AS di ON (ii.`itemEntry`=di.`col_0`)
			LEFT JOIN `dbc_itemdisplayinfo` AS idi ON (di.`col_5`=idi.`col_0`)
			WHERE ii.`guid`=?',
			array($guid)
		);

		if ($get_item->rowCount() == 1) {
			$this->_item = $get_item->fetch(PDO::FETCH_ASSOC);
			$this->_calc_itemscore();
			$this->_random_enchant();	// check if random enchanted and if so, process it
			$this->_parse_enchants();
			$this->_process();
			$this->store_cache(array('item_guid',$guid),$this->_item);
		}
	}

	/**
	 * Get item by entry. Some values will be empty (enchants, actual durability, ...)
	 */
	public function get_by_entry($entry) {
		if ($this->_item = $this->get_cache(array('item_entry',$entry),ITEM_EXPIRE)) {
			$this->_process();
			return;
		}

		$get_item = $this->db->query('
			SELECT "1" AS `count`,"" AS `enchantments`,dis.`col_0` AS `itemEntry`,"0" AS randomPropertyId,"0" AS durability,dis.`col_1` AS quality,dis.`col_2` AS flags,dis.`col_3` AS flags2,dis.`col_8` AS SellPrice,dis.`col_9` AS inventoryType,dis.`col_10` AS allowableClass,dis.`col_11` AS allowableRace,dis.`col_12` AS itemLevel,dis.`col_13` AS requiredLevel,dis.`col_14` AS requiredSkill,dis.`col_15` AS requiredSkillRank,dis.`col_16` AS requiredSpell,dis.`col_17` AS requiredHonorRank,dis.`col_18` AS requiredCityRank,dis.`col_19` AS requiredReputationFaction,dis.`col_20` AS requiredReputationRank,dis.`col_23` AS ContainerSlots,dis.`col_24` AS statType_0,dis.`col_25` AS statType_1,dis.`col_26` AS statType_2,dis.`col_27` AS statType_3,dis.`col_28` AS statType_4,dis.`col_29` AS statType_5,dis.`col_30` AS statType_6,dis.`col_31` AS statType_7,dis.`col_32` AS statType_8,dis.`col_33` AS statType_9,dis.`col_34` AS statValue_0,dis.`col_35` AS statValue_1,dis.`col_36` AS statValue_2,dis.`col_37` AS statValue_3,dis.`col_38` AS statValue_4,dis.`col_39` AS statValue_5,dis.`col_40` AS statValue_6,dis.`col_41` AS statValue_7,dis.`col_42` AS statValue_8,dis.`col_43` AS statValue_9,dis.`col_66` AS speed,dis.`col_68` AS spell_1,dis.`col_69` AS spell_2,dis.`col_70` AS spell_3,dis.`col_71` AS spell_4,dis.`col_72` AS spell_5,dis.`col_98` AS bonding,dis.`col_99` AS name,dis.`col_113` AS item_set,dis.`col_118` AS socket_1,dis.`col_119` AS socket_2,dis.`col_120` AS socket_3,dis.`col_124` AS socket_bonus,LOWER(idi.`col_5`) AS icon,di.`col_1` AS itemClass,di.`col_2` AS itemSubClass
			FROM `db2_item_sparse` AS dis
			LEFT JOIN `db2_item` AS di ON (dis.`col_0`=di.`col_0`)
			LEFT JOIN `dbc_itemdisplayinfo` AS idi ON (di.`col_5`=idi.`col_0`)
			WHERE dis.`col_0`=?',
			array($entry)
		);

		if ($get_item->rowCount() == 1) {
			$this->_item = $get_item->fetch(PDO::FETCH_ASSOC);
			$this->_calc_itemscore();
			$this->_process();
			$this->store_cache(array('item_entry',$entry),$this->_item);
		}
	}

	/**
	 * Set item properties
	 */
	private function _process() {
		$this->entry = $this->_item['itemEntry'];
		$this->name = $this->_item['name'];
		$this->flags = $this->_item['flags'];
		$this->flags2 = $this->_item['flags2'];
		$this->quality = $this->_item['quality'];
		$this->icon = $this->_item['icon'];
		$this->class = $this->_item['itemClass'];
		$this->subclass = $this->_item['itemSubClass'];
		$this->inventory_type = $this->_item['inventoryType'];
		$this->item_level = $this->_item['itemLevel'];
		$this->speed = $this->_item['speed'] / 1000;
		$this->score = $this->_item['score'];
	}

	/**
	 * Returns item informations
	 * @return array item informations
	 */
	public function get_item() {
		if (!$this->_item['itemEntry'])
			return FALSE;

		return $this->_item;
	}

	/**
	 * Returns item maximum durability
	 * @return integer durability
	 */
	public function get_max_durability() {
		global $shared;

		// this is trinitycore code from server/game/Globals/ObjectMgr.cpp rewritten to PHP
		// uint32 FillMaxDurability(uint32 itemClass, uint32 itemSubClass, uint32 inventoryType, uint32 quality, uint32 itemLevel)

		$levelPenalty = 1.0;
		if ($this->item_level <= 28)
			$levelPenalty = 0.966 - (28 - $this->item_level) / 54.0;

		if ($this->class == ITEM_CLASS_ARMOR) {
			if ($this->item_type > INVTYPE_ROBE)
				return 0;
			return (5 * intval(23.0 * $shared['qualityMultipliers'][$this->quality] * $shared['armorMultipliers'][$this->inventory_type] * $levelPenalty + 0.5));
		}

		return (5 * intval(17.0 * $shared['qualityMultipliers'][$this->quality] * $shared['weaponMultipliers'][$this->subclass] * $levelPenalty + 0.5));
	}

	/**
	 * Returns item armor value
	 * @return integer armor
	 */
	public function get_armor() {

		// this is trinitycore code from server/game/Globals/ObjectMgr.cpp rewritten to PHP
		// uint32 FillItemArmor(uint32 itemlevel, uint32 itemClass, uint32 itemSubclass, uint32 quality, uint32 inventoryType)

		if ($this->quality > ITEM_QUALITY_ARTIFACT)
		        return 0;

		if ($this->class != ITEM_CLASS_ARMOR || $this->subclass != ITEM_SUBCLASS_ARMOR_SHIELD) {
			$get_armor_quality = $this->db->query('
				SELECT `col_1`,`col_2`,`col_3`,`col_4`,`col_5`,`col_6`,`col_7`,`col_8`
				FROM `dbc_itemarmorquality`
				WHERE `col_0`=?',
				array($this->item_level)	// poor, common, uncommon, rare, epic, legendary, artifact, heirloom
			);
			$armor_quality = $get_armor_quality->fetch(PDO::FETCH_ASSOC);
			$quality = $armor_quality['col_'.($this->quality+1)];


			$get_armor_total = $this->db->query('
				SELECT `col_2`,`col_3`,`col_4`,`col_5`
				FROM `dbc_itemarmortotal`
				WHERE `col_0`=?',
				array($this->item_level)	// cloth, leather, mail, plate
			);
			$armor_total = $get_armor_total->fetch(PDO::FETCH_ASSOC);
			$total = $armor_total['col_'.($this->subclass+1)];

			if (!$quality || !$total)
				return 0;

			$inventory_type = $this->inventory_type;
			if ($inventory_type == INVTYPE_ROBE)
				$inventory_type = INVTYPE_CHEST;			

			$get_armor_location = $this->db->query('
				SELECT `col_2`,`col_3`,`col_4`,`col_5`
				FROM `dbc_armorlocation`
				WHERE `col_0`=?',
				array($inventory_type)	// cloth, leather, mail, plate
			);
			$armor_location = $get_armor_location->fetch(PDO::FETCH_ASSOC);
			$location = $armor_location['col_'.($this->subclass+1)];

			if (!$location)
				return 0;
			
			if ($this->subclass < ITEM_SUBCLASS_ARMOR_CLOTH)
				return 0;

			return intval($quality * $total * $location + 0.5);
		} else {

			$get_armor_shield = $this->db->query('
				SELECT `col_1`,`col_2`,`col_3`,`col_4`,`col_5`,`col_6`,`col_7`,`col_8`
				FROM `dbc_itemarmorshield`
				WHERE `col_0`=?',
				array($this->item_level)  // poor, common, uncommon, rare, epic, legendary, artifact, heirloom
			);
			$armor_shield = $get_armor_shield->fetch(PDO::FETCH_ASSOC);
			$shield = $armor_shield['col_'.($this->quality+1)];

			return intval($shield + 0.5);
		}
	}

	/**
	 * Fetches damage from appropiete dbc store
	 * @return float damage 
	 */
	private function _get_dps($store) {
		$get_damage = $this->db->query('
			SELECT `col_1`,`col_2`,`col_3`,`col_4`,`col_5`,`col_6`,`col_7`,`col_8`
			FROM `dbc_itemdamage'.$store.'`
			WHERE `col_0`=?',
			array($this->item_level)  // poor, common, uncommon, rare, epic, legendary, artifact, heirloom
		);
		$damage = $get_damage->fetch(PDO::FETCH_ASSOC);
		return $damage['col_'.($this->quality+1)];
	}

	/**
	 * Returns item min, max and avg damage
	 * @return array damage
	 */
	public function get_damage() {
		//void FillItemDamageFields(float* minDamage, float* maxDamage, float* dps, uint32 itemLevel, uint32 itemClass, uint32 itemSubClass, uint32 quality, uint32 delay, float statScalingFactor, uint32 inventoryType,

		if ($this->class != ITEM_CLASS_WEAPON || $this->quality > ITEM_QUALITY_ARTIFACT)
			return;

		$dps = 0;
		switch ($this->inventory_type) {

			case INVTYPE_AMMO:
				$dps = $this->_get_dps('ammo');
				break;
			case INVTYPE_2HWEAPON:
				if ($this->flags2 & ITEM_FLAGS_EXTRA_CASTER_WEAPON)
					$dps = $this->_get_dps('twohandcaster');
				else
					$dps = $this->_get_dps('twohand');
				break;
			case INVTYPE_RANGED:
			case INVTYPE_THROWN:
			case INVTYPE_RANGEDRIGHT:
				switch ($this->subclass){
					case ITEM_SUBCLASS_WEAPON_WAND:
						$dps = $this->_get_dps('wand');
						break;
					case ITEM_SUBCLASS_WEAPON_THROWN:
						$dps = $this->_get_dps('thrown');
						break;
					case ITEM_SUBCLASS_WEAPON_BOW:
					case ITEM_SUBCLASS_WEAPON_GUN:
					case ITEM_SUBCLASS_WEAPON_CROSSBOW:
						$dps = $this->_get_dps('ranged');
						break;
					default:
						return;
				}
				break;
			case INVTYPE_WEAPON:
			case INVTYPE_WEAPONMAINHAND:
			case INVTYPE_WEAPONOFFHAND:
				if ($this->flags2 & ITEM_FLAGS_EXTRA_CASTER_WEAPON)
					$dps = $this->_get_dps('onehandcaster');
				else
					$dps = $this->_get_dps('onehand');
				break;
			default:
				return;
		}

		$avg_damage = sprintf("%.3f",($dps * $this->speed));
		$min_damage = intval($avg_damage - (0.20 * $avg_damage));
		$max_damage = intval($avg_damage + ($avg_damage * 0.20));

		return array( 'dps' => sprintf("%.1f",$dps), 'min' => $min_damage, 'max' => $max_damage );
	}

	/**
	 * Calculates a GearScore for item - Primitive formula (TODO: stats weight)
	 */
	private function _calc_itemscore() {
		global $shared;
		$itemscore = floor(((($this->_item['itemLevel'] - $shared['formula'][$this->_item['quality']][0]) / $shared['formula'][$this->_item['quality']][1]) * $shared['SlotMod'][$this->_item['inventoryType']]) * 1.8291);
		if ($itemscore < 0)
			$itemscore = 0;
		$this->_item['score'] = $itemscore;
	}

	private function _parse_enchants() {
		$raw_enchantments = explode(' ',$this->_item['enchantments']);
		for ($i=0;$i<=14;++$i) {
			$this->_item['enchants'][$i] = array();
			for ($j=0;$j<=2;++$j) {
				$this->_item['enchants'][$i][$j] = $raw_enchantments[$i*3+$j];
				if ($j == 0) {
					if ($raw_enchantments[$i*3+$j] != 0) {
						$base_value = NULL;
						// random enchantments - base value calculation
						if ($i >= 10) {
							$base_value = intval($this->_item['properties']['factor'] * $this->_item['properties']['prefix_'.$i] / 10000);
						}
						$enchant = new SpellItemEnchantment($this->db,$raw_enchantments[$i*3+$j],$base_value);
						$this->_item['enchants'][$i][3] = $enchant->get_name();
					}
				}
			}
		}
	}


	public function get_enchant($what) {
		if (array_key_exists($what,$this->_item['enchants']))
			return $this->_item['enchants'][$what];
	}


	private function _random_enchant() {
		if ($this->_item['randomPropertyId'] == 0)
			return;


		if ($this->_item['randomPropertyId'] > 0) { // property
			$get_random_properties = $this->db->query('
				SELECT `col_7` AS suffix
				FROM `dbc_itemrandomproperties`
				WHERE `col_0`=?',
				array($this->_item['randomPropertyId'])
			);
			$properties = $get_random_properties->fetch(PDO::FETCH_ASSOC);
			$this->_item['name'] .= ' '.$properties['suffix'];

		} else if ($this->_item['randomPropertyId'] < 0) { // suffix



			$suffixFactor = 0;
			switch ($this->_item['inventoryType']) {
				case INVTYPE_HEAD:
				case INVTYPE_BODY:
				case INVTYPE_CHEST:
				case INVTYPE_LEGS:
				case INVTYPE_2HWEAPON:
				case INVTYPE_ROBE:
					$suffixFactor = 1;
					break;
				case INVTYPE_SHOULDERS:
				case INVTYPE_WAIST:
				case INVTYPE_FEET:
				case INVTYPE_HANDS:
				case INVTYPE_TRINKET:
					$suffixFactor = 2;
					break;
				case INVTYPE_NECK:
				case INVTYPE_WRISTS:
				case INVTYPE_FINGER:
				case INVTYPE_SHIELD:
				case INVTYPE_CLOAK:
				case INVTYPE_HOLDABLE:
					$suffixFactor = 3;
					break;
				case INVTYPE_WEAPON:
				case INVTYPE_WEAPONMAINHAND:
				case INVTYPE_WEAPONOFFHAND:
					$suffixFactor = 4;
					break;
				case INVTYPE_RANGED:
				case INVTYPE_THROWN:
				case INVTYPE_RANGEDRIGHT:
					$suffixFactor = 5;
					break;
			}

			$column = 0;
			switch ($this->_item['quality']) {
				case 2:
					$column = 10;
					break;
				case 3:
					$column = 5;
					break;
				case 4:
					$column = 0;
					break;
			}

			$get_random_points = $this->db->query('
				SELECT `col_'.($suffixFactor+$column).'` AS factor
				FROM `dbc_randproppoints`
				WHERE `col_0`=?',
				array($this->_item['itemLevel'])
			);
			$f = $get_random_points->fetch(PDO::FETCH_ASSOC);
			$factor = $f['factor'];
			
			$get_random_suffix = $this->db->query('
				SELECT `col_1` AS suffix,`col_3`,`col_4`,`col_5`,`col_6`,`col_7`,`col_8`,`col_9`,`col_10`,`col_11`,`col_12`
				FROM `dbc_itemrandomsuffix`
				WHERE `col_0`=?',
				array(abs($this->_item['randomPropertyId']))
			);
			$properties = $get_random_suffix->fetch(PDO::FETCH_ASSOC);
			$this->_item['properties'] = array(
				'enchant_10' => $properties['col_3'],
				'enchant_11' => $properties['col_4'],
				'enchant_12' => $properties['col_5'],
				'enchant_13' => $properties['col_6'],
				'enchant_14' => $properties['col_7'],
				'prefix_10' => $properties['col_8'],
				'prefix_11' => $properties['col_9'],
				'prefix_12' => $properties['col_10'],
				'prefix_13' => $properties['col_11'],
				'prefix_14' => $properties['col_12'],
				'factor' => $factor
			);
			$this->_item['name'] .= ' '.$properties['suffix'];
		}
	}
}
