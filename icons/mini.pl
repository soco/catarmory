#!/usr/bin/perl

#
# Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.
#

opendir(DIR, '.') || die "can't opendir: $!";
my @pngs = grep { !/^\./ && -f "./$_" && $_ =~ /\.png$/i} readdir(DIR);
closedir DIR;

my $count = scalar(@pngs);

foreach my $size (32,14) {
   mkdir "../icons_${size}";
   my $num = 0;
   my $prg = 0;
   print 'Converting icons from 64x64 -> '.$size.'x'.$size."\n";
   print 'Progress: 0% --------------------------------------------------------------------------------------------------- 100%'."\n";
   print '             ';              
   foreach my $file (@pngs) {
      my $progress = int($num / $count * 100);
      if ($progress > $prg) {
         print '#';
      }
      $prg = $progress;
                                          
      my $null = `/usr/bin/pngtopnm "${file}" > /tmp/temp.pnm 2>/dev/null`;
      $null = `/usr/bin/pnmscale -xysize ${size} ${size} /tmp/temp.pnm > resized_temp.pnm 2>/dev/null`;
      $null = `/usr/bin/pnmtopng resized_temp.pnm > "../icons_${size}/${file}" 2>/dev/null`;
      unlink '/tmp/temp.pnm';
      unlink 'resized_temp.pnm';
      ++$num;
   }
   print "\n";
}
