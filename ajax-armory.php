<?php


/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


include_once "configs/config.php";
include_once "includes/database.php";
include_once "includes/cache.php";
include_once "includes/auth.php";
include_once "includes/shared.php";
include_once "includes/loot.php";
include_once "includes/character.php";
include_once "includes/search.php";
include_once "includes/item.php";
include_once "includes/arena.php";
include_once "includes/guild.php";
include_once "includes/quest.php";
include_once "includes/faction.php";
include_once "includes/npc.php";
include_once "includes/spell.php";
include_once "includes/talents.php";
include_once "includes/glyph.php";
include_once "includes/achievement.php";

class Armory {

	public $error_code = 0;
	public $error_text = '';
	public $output = array();	// output before json encoding

	private $_callback = '';	// jsonp callback
	private $_dbh;				// database handler
	private $_cached;

	function __construct() {

	}

	public function init() {
		$this->_callback = $_GET['callback'];
		$this->db = new Database($this);

		// check if there is no error (mainly database connection error)
		if ($this->error_code == 0) {

			$this->auth = new Auth($this->db,$this);

			switch ($_GET['what']) {
				case 'char':
					$this->character = new Character($this->db,(array_key_exists('guid',$_GET) ? $_GET['guid'] : $_GET['name']),$this);
					switch ($_GET['action']) {
						case 'basic':
							$this->_output = $this->character->get_char();
							break;
						case 'inventory':
							$this->_output = $this->character->get_char_items();
							break;
						case 'skills':
							$this->_output = $this->character->get_char_skills();
							break;
						case 'talents':
							$this->_output = $this->character->get_char_talents();
							break;
						case 'quests':		// finished quests in zone
							$this->_output = $this->character->get_completed_quests($_GET['zone']);
							break;
						case 'achievements':	// finished achievements in category
							$this->_output = $this->character->get_completed_achievements($_GET['category']);
							break;
						case 'mailbox':		// user mailbox (only to authenticated owners)
							$this->_output = $this->character->get_char_mailbox();
							break;
						case 'auctions':	// user auctions (only to authenticated owners)
							$this->_output = $this->character->get_char_auctions();
							break;
					}

					break;

				case 'search':
					$search = new Search($this->db);
					$this->_output = $search->results($_GET['string']);
					break;

				case 'item':
					$item = new Item($this->db);
					$item->get_by_entry($_GET['id']);
					$this->_output = $item->get_item();
					break;

				case 'arenateam':
					$arena_team = new Arenateam($this->db);
					if (array_key_exists('id',$_GET)) {
						$arena_team->get_by_guid($_GET['id']);
					} else if (array_key_exists('name',$_GET)) {
						$arena_team->get_by_name($_GET['name']);
					}

					switch ($_GET['action']) {
						case 'basic':
							$this->_output = $arena_team->get_team();
							break;
					}
					break;

				case 'guild':
					$guild = new Guild($this->db,$this);
					if (array_key_exists('guildid',$_GET)) {
						$guild->get_by_guid($_GET['id']);
					} else if (array_key_exists('name',$_GET)) {
						$guild->get_by_name($_GET['name']);
					}

					switch ($_GET['action']) {
						case 'basic':
							$this->_output = $guild->get_guild();
							break;
						case 'members':
							$this->_output = $guild->get_members();
							break;
						case 'guildbank':
							$this->_output = $guild->get_guildbank();
							break;
						case 'achievements':	// finished achievements in category
							$this->_output = $guild->get_achievements($_GET['category']);
							break;
					}
					break;

				case 'quest':
					$quest = new Quest($this->db,$_GET['id']);
					$this->_output = $quest->get_quest();
					break;

				case 'quests':
					$quests = new Quests($this->db);
					switch ($_GET['action']) {
						case 'by_zone':
							$this->_output = $quests->search_by_zone($_GET['zone']);
							break;
					}
					break;

				case 'npc':
					$npc = new Npc($this->db,$_GET['id']);
					switch ($_GET['action']) {
						case 'basic':
							$this->_output = $npc->get_npc();
							break;
						case 'spawns':
							$this->_output = $npc->get_spawns();
							break;

					}
					break;
				case 'spell':
					$spell = new Spell($this->db,$_GET['id']);
					$this->_output = $spell->get_spell();
					break;

				case 'achievement':
					$achievement = new Achievement($this->db,$_GET['id']);
					$this->_output = $achievement->get_achievement();
					$this->_output['achievers'] = $achievement->get_achievers();
					
					break;

				// get talents template of class (this is being cached as it is not changing - dbc values). In talent calculator it is combined with Character::get_char_talents()
				case 'talents':
					$talents = new Talents($this->db);
					$this->_output = $talents->get_talents_tabs($_GET['class']);
					break;

				// get achievements categories for character achievements (can be also guild and statistics)
				case 'achievements':
					$achievements = new Achievements($this->db);

					switch ($_GET['action']) {
						case 'char':
							$this->_output = $achievements->get_char_categories();
							break;
						case 'by_category':
							$this->_output = $achievements->search_by_category($_GET['category']);
							break;
						case 'guild':
							$this->_output = $achievements->get_guild_categories();
							break;
					}
					break;
				// users authorization
				case 'auth':
					switch ($_GET['action']) {
						case 'challenge':
							$this->_output = $this->auth->challenge($_GET['login']);
							break;
						case 'response':
							$this->_output = $this->auth->response($_GET['challenge_id'],$_GET['response']);
							break;
						case 'check':
							$this->_output = $this->auth->check();
							break;
						case 'set_profile':
							// this function can be extended to support more profile settings later
							$this->_output = $this->auth->set_profile($_GET['nick']);
							break;
						case 'logout':
							$this->_output = $this->auth->logout();
							break;

					}
					$this->set_cached(false);
					break;

				default: 
					$this->_throw_error(509,"Request Error","Provided request doesn't match any of internal methods");
					break;
				
			}
		}


		$this->output();
	}

	public function set_cached($cached) {
		$this->_cached = $cached;
	}

	public function throw_error($code,$category,$text) {
		$this->error_code = $code;
		$this->error_category = $category;
		$this->error_text = $text;
	}


	private function output() {
		if ($this->error_code != 0) {
			$error = array(
				'code' => $this->error_code,
				'category' => $this->error_category,
				'text' => $this->error_text
			);
			print $this->_callback.'('. json_encode(array('error' => $error)) .');';
		} else {
			$data = array(
				'data' => $this->_output,
				'cached' => $this->_cached,
				'logged' => $this->auth->loggedState()
			);

			print $this->_callback.'('. json_encode($data) .');';
		}
	}
}

error_reporting(0);
session_start();
header('Content-Type: text/javascript; charset=utf8');

$armory = new Armory();
$armory->init();
